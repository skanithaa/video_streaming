package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllCategory {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("see_all_url")
    @Expose
    private String seeAllUrl;
    @SerializedName("data")
    @Expose
    private List<CategoryItems> categoryItems = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeeAllUrl() {
        return seeAllUrl;
    }

    public void setSeeAllUrl(String seeAllUrl) {
        this.seeAllUrl = seeAllUrl;
    }

    public List<CategoryItems> getCategoryItems() {
        return categoryItems;
    }

    public void setCategoryItems(List<CategoryItems> categoryItems) {
        this.categoryItems = categoryItems;
    }
}
