package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {


        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private UserLoginDataResponse userLoginDataResponse;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public UserLoginDataResponse getData() {
            return userLoginDataResponse;
        }

        public void setData(UserLoginDataResponse userLoginDataResponse) {
            this.userLoginDataResponse = userLoginDataResponse;
        }


    }

