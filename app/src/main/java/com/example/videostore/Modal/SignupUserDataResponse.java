package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignupUserDataResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("token_expiry")
    @Expose
    private String tokenExpiry;
    @SerializedName("social_unique_id")
    @Expose
    private String socialUniqueId;
    @SerializedName("login_by")
    @Expose
    private String loginBy;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("card_id")
    @Expose
    private String cardId;
    @SerializedName("user_status")
    @Expose
    private Integer userStatus;
    @SerializedName("email_notification")
    @Expose
    private Integer emailNotification;
    @SerializedName("push_status")
    @Expose
    private Integer pushStatus;
    @SerializedName("is_verified")
    @Expose
    private Integer isVerified;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("card_last_four_number")
    @Expose
    private String cardLastFourNumber;
    @SerializedName("sub_profile_id")
    @Expose
    private Integer subProfileId;
    @SerializedName("sub_profile_name")
    @Expose
    private String subProfileName;
    @SerializedName("sub_profile_picture")
    @Expose
    private String subProfilePicture;
    @SerializedName("appstore_update_status")
    @Expose
    private Integer appstoreUpdateStatus;
    @SerializedName("payment_subscription")
    @Expose
    private Integer paymentSubscription;
    @SerializedName("verification_control")
    @Expose
    private String verificationControl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenExpiry() {
        return tokenExpiry;
    }

    public void setTokenExpiry(String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    public String getSocialUniqueId() {
        return socialUniqueId;
    }

    public void setSocialUniqueId(String socialUniqueId) {
        this.socialUniqueId = socialUniqueId;
    }

    public String getLoginBy() {
        return loginBy;
    }

    public void setLoginBy(String loginBy) {
        this.loginBy = loginBy;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(Integer emailNotification) {
        this.emailNotification = emailNotification;
    }

    public Integer getPushStatus() {
        return pushStatus;
    }

    public void setPushStatus(Integer pushStatus) {
        this.pushStatus = pushStatus;
    }

    public Integer getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Integer isVerified) {
        this.isVerified = isVerified;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCardLastFourNumber() {
        return cardLastFourNumber;
    }

    public void setCardLastFourNumber(String cardLastFourNumber) {
        this.cardLastFourNumber = cardLastFourNumber;
    }

    public Integer getSubProfileId() {
        return subProfileId;
    }

    public void setSubProfileId(Integer subProfileId) {
        this.subProfileId = subProfileId;
    }

    public String getSubProfileName() {
        return subProfileName;
    }

    public void setSubProfileName(String subProfileName) {
        this.subProfileName = subProfileName;
    }

    public String getSubProfilePicture() {
        return subProfilePicture;
    }

    public void setSubProfilePicture(String subProfilePicture) {
        this.subProfilePicture = subProfilePicture;
    }

    public Integer getAppstoreUpdateStatus() {
        return appstoreUpdateStatus;
    }

    public void setAppstoreUpdateStatus(Integer appstoreUpdateStatus) {
        this.appstoreUpdateStatus = appstoreUpdateStatus;
    }

    public Integer getPaymentSubscription() {
        return paymentSubscription;
    }

    public void setPaymentSubscription(Integer paymentSubscription) {
        this.paymentSubscription = paymentSubscription;
    }

    public String getVerificationControl() {
        return verificationControl;
    }

    public void setVerificationControl(String verificationControl) {
        this.verificationControl = verificationControl;
    }
}
