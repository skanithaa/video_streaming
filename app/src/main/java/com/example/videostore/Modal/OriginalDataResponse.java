package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OriginalDataResponse {

    @SerializedName("admin_video_id")
    @Expose
    private Integer adminVideoId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("sub_category_id")
    @Expose
    private Integer subCategoryId;
    @SerializedName("genre_id")
    @Expose
    private Integer genreId;
    @SerializedName("video_type")
    @Expose
    private Integer videoType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("default_image")
    @Expose
    private String defaultImage;
    @SerializedName("mobile_image")
    @Expose
    private String mobileImage;
    @SerializedName("banner_image")
    @Expose
    private String bannerImage;
    @SerializedName("mobile_banner_image")
    @Expose
    private String mobileBannerImage;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("publish_time")
    @Expose
    private String publishTime;
    @SerializedName("watch_count")
    @Expose
    private Integer watchCount;
    @SerializedName("watch_count_formatted")
    @Expose
    private String watchCountFormatted;

    public Integer getAdminVideoId() {
        return adminVideoId;
    }

    public void setAdminVideoId(Integer adminVideoId) {
        this.adminVideoId = adminVideoId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public Integer getVideoType() {
        return videoType;
    }

    public void setVideoType(Integer videoType) {
        this.videoType = videoType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(String defaultImage) {
        this.defaultImage = defaultImage;
    }

    public String getMobileImage() {
        return mobileImage;
    }

    public void setMobileImage(String mobileImage) {
        this.mobileImage = mobileImage;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getMobileBannerImage() {
        return mobileBannerImage;
    }

    public void setMobileBannerImage(String mobileBannerImage) {
        this.mobileBannerImage = mobileBannerImage;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public Integer getWatchCount() {
        return watchCount;
    }

    public void setWatchCount(Integer watchCount) {
        this.watchCount = watchCount;
    }

    public String getWatchCountFormatted() {
        return watchCountFormatted;
    }

    public void setWatchCountFormatted(String watchCountFormatted) {
        this.watchCountFormatted = watchCountFormatted;
    }
}
