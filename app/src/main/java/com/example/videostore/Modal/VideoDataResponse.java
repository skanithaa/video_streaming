package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoDataResponse {
    @SerializedName("admin_video_id")
    @Expose
    private Integer adminVideoId;
    @SerializedName("admin_unique_id")
    @Expose
    private String adminUniqueId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("sub_category_id")
    @Expose
    private Integer subCategoryId;
    @SerializedName("genre_id")
    @Expose
    private Integer genreId;
    @SerializedName("video_type")
    @Expose
    private Integer videoType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("default_image")
    @Expose
    private String defaultImage;
    @SerializedName("mobile_image")
    @Expose
    private String mobileImage;
    @SerializedName("banner_image")
    @Expose
    private String bannerImage;
    @SerializedName("mobile_banner_image")
    @Expose
    private String mobileBannerImage;
    @SerializedName("publish_time")
    @Expose
    private String publishTime;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("video_subtitle")
    @Expose
    private String videoSubtitle;
    @SerializedName("video_subtitle_vtt")
    @Expose
    private String videoSubtitleVtt;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("trailer_video")
    @Expose
    private String trailerVideo;
    @SerializedName("trailer_subtitle")
    @Expose
    private String trailerSubtitle;
    @SerializedName("trailer_subtitle_vtt")
    @Expose
    private String trailerSubtitleVtt;
    @SerializedName("trailer_duration")
    @Expose
    private String trailerDuration;
    @SerializedName("ratings")
    @Expose
    private String ratings;
    @SerializedName("reviews")
    @Expose
    private String reviews;
    @SerializedName("video_resolutions")
    @Expose
    private String videoResolutions;
    @SerializedName("trailer_video_resolutions")
    @Expose
    private String trailerVideoResolutions;
    @SerializedName("video_resize_path")
    @Expose
    private String videoResizePath;
    @SerializedName("trailer_resize_path")
    @Expose
    private Object trailerResizePath;
    @SerializedName("watch_count")
    @Expose
    private Integer watchCount;
    @SerializedName("is_pay_per_view")
    @Expose
    private Integer isPayPerView;
    @SerializedName("type_of_user")
    @Expose
    private Integer typeOfUser;
    @SerializedName("type_of_subscription")
    @Expose
    private Integer typeOfSubscription;
    @SerializedName("ppv_amount")
    @Expose
    private Integer ppvAmount;
    @SerializedName("video_upload_type")
    @Expose
    private Integer videoUploadType;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("is_kids_video")
    @Expose
    private Integer isKidsVideo;
    @SerializedName("download_status")
    @Expose
    private Integer downloadStatus;
    @SerializedName("player_json")
    @Expose
    private String playerJson;
    @SerializedName("hls_main_video")
    @Expose
    private String hlsMainVideo;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("share_link")
    @Expose
    private String shareLink;
    @SerializedName("share_message")
    @Expose
    private String shareMessage;
    @SerializedName("seek_time_in_seconds")
    @Expose
    private String seekTimeInSeconds;
    @SerializedName("video_play_duration")
    @Expose
    private String videoPlayDuration;
    @SerializedName("wishlist_status")
    @Expose
    private Integer wishlistStatus;
    @SerializedName("history_status")
    @Expose
    private Integer historyStatus;
    @SerializedName("is_liked")
    @Expose
    private Integer isLiked;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("dislikes")
    @Expose
    private String dislikes;
    @SerializedName("likes_formatted")
    @Expose
    private String likesFormatted;
    @SerializedName("dislikes_formatted")
    @Expose
    private String dislikesFormatted;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("main_video")
    @Expose
    private String mainVideo;
    @SerializedName("is_series")
    @Expose
    private Integer isSeries;
    @SerializedName("should_display_ppv")
    @Expose
    private Integer shouldDisplayPpv;
    @SerializedName("ppv_page_type")
    @Expose
    private Integer ppvPageType;
    /*@SerializedName("ppv_page_type_content")
    @Expose
    private List<PpvPageTypeContent> ppvPageTypeContent = null;*/
    @SerializedName("is_user_need_subscription")
    @Expose
    private Integer isUserNeedSubscription;
    /*@SerializedName("images")
    @Expose
    private List<Object> images = null;
    @SerializedName("cast_crews")
    @Expose
    private List<Object> castCrews = null;*/
    @SerializedName("download_button_status")
    @Expose
    private Integer downloadButtonStatus;
    @SerializedName("downloading_video_status")
    @Expose
    private Integer downloadingVideoStatus;
    @SerializedName("download_status_text")
    @Expose
    private String downloadStatusText;
    /*@SerializedName("main_resolutions")
    @Expose
    private MainResolutions mainResolutions;
    @SerializedName("download_urls")
    @Expose
    private List<DownloadUrl> downloadUrls = null;*/
    @SerializedName("next_admin_video_id")
    @Expose
    private Integer nextAdminVideoId;
    @SerializedName("next_player_json")
    @Expose
    private String nextPlayerJson;
    @SerializedName("watch_count_formatted")
    @Expose
    private String watchCountFormatted;

    public Integer getAdminVideoId() {
        return adminVideoId;
    }

    public void setAdminVideoId(Integer adminVideoId) {
        this.adminVideoId = adminVideoId;
    }

    public String getAdminUniqueId() {
        return adminUniqueId;
    }

    public void setAdminUniqueId(String adminUniqueId) {
        this.adminUniqueId = adminUniqueId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public Integer getVideoType() {
        return videoType;
    }

    public void setVideoType(Integer videoType) {
        this.videoType = videoType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(String defaultImage) {
        this.defaultImage = defaultImage;
    }

    public String getMobileImage() {
        return mobileImage;
    }

    public void setMobileImage(String mobileImage) {
        this.mobileImage = mobileImage;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getMobileBannerImage() {
        return mobileBannerImage;
    }

    public void setMobileBannerImage(String mobileBannerImage) {
        this.mobileBannerImage = mobileBannerImage;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideoSubtitle() {
        return videoSubtitle;
    }

    public void setVideoSubtitle(String videoSubtitle) {
        this.videoSubtitle = videoSubtitle;
    }

    public String getVideoSubtitleVtt() {
        return videoSubtitleVtt;
    }

    public void setVideoSubtitleVtt(String videoSubtitleVtt) {
        this.videoSubtitleVtt = videoSubtitleVtt;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTrailerVideo() {
        return trailerVideo;
    }

    public void setTrailerVideo(String trailerVideo) {
        this.trailerVideo = trailerVideo;
    }

    public String getTrailerSubtitle() {
        return trailerSubtitle;
    }

    public void setTrailerSubtitle(String trailerSubtitle) {
        this.trailerSubtitle = trailerSubtitle;
    }

    public String getTrailerSubtitleVtt() {
        return trailerSubtitleVtt;
    }

    public void setTrailerSubtitleVtt(String trailerSubtitleVtt) {
        this.trailerSubtitleVtt = trailerSubtitleVtt;
    }

    public String getTrailerDuration() {
        return trailerDuration;
    }

    public void setTrailerDuration(String trailerDuration) {
        this.trailerDuration = trailerDuration;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getVideoResolutions() {
        return videoResolutions;
    }

    public void setVideoResolutions(String videoResolutions) {
        this.videoResolutions = videoResolutions;
    }

    public String getTrailerVideoResolutions() {
        return trailerVideoResolutions;
    }

    public void setTrailerVideoResolutions(String trailerVideoResolutions) {
        this.trailerVideoResolutions = trailerVideoResolutions;
    }

    public String getVideoResizePath() {
        return videoResizePath;
    }

    public void setVideoResizePath(String videoResizePath) {
        this.videoResizePath = videoResizePath;
    }

    public Object getTrailerResizePath() {
        return trailerResizePath;
    }

    public void setTrailerResizePath(Object trailerResizePath) {
        this.trailerResizePath = trailerResizePath;
    }

    public Integer getWatchCount() {
        return watchCount;
    }

    public void setWatchCount(Integer watchCount) {
        this.watchCount = watchCount;
    }

    public Integer getIsPayPerView() {
        return isPayPerView;
    }

    public void setIsPayPerView(Integer isPayPerView) {
        this.isPayPerView = isPayPerView;
    }

    public Integer getTypeOfUser() {
        return typeOfUser;
    }

    public void setTypeOfUser(Integer typeOfUser) {
        this.typeOfUser = typeOfUser;
    }

    public Integer getTypeOfSubscription() {
        return typeOfSubscription;
    }

    public void setTypeOfSubscription(Integer typeOfSubscription) {
        this.typeOfSubscription = typeOfSubscription;
    }

    public Integer getPpvAmount() {
        return ppvAmount;
    }

    public void setPpvAmount(Integer ppvAmount) {
        this.ppvAmount = ppvAmount;
    }

    public Integer getVideoUploadType() {
        return videoUploadType;
    }

    public void setVideoUploadType(Integer videoUploadType) {
        this.videoUploadType = videoUploadType;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getIsKidsVideo() {
        return isKidsVideo;
    }

    public void setIsKidsVideo(Integer isKidsVideo) {
        this.isKidsVideo = isKidsVideo;
    }

    public Integer getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(Integer downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public String getPlayerJson() {
        return playerJson;
    }

    public void setPlayerJson(String playerJson) {
        this.playerJson = playerJson;
    }

    public String getHlsMainVideo() {
        return hlsMainVideo;
    }

    public void setHlsMainVideo(String hlsMainVideo) {
        this.hlsMainVideo = hlsMainVideo;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public String getShareMessage() {
        return shareMessage;
    }

    public void setShareMessage(String shareMessage) {
        this.shareMessage = shareMessage;
    }

    public String getSeekTimeInSeconds() {
        return seekTimeInSeconds;
    }

    public void setSeekTimeInSeconds(String seekTimeInSeconds) {
        this.seekTimeInSeconds = seekTimeInSeconds;
    }

    public String getVideoPlayDuration() {
        return videoPlayDuration;
    }

    public void setVideoPlayDuration(String videoPlayDuration) {
        this.videoPlayDuration = videoPlayDuration;
    }

    public Integer getWishlistStatus() {
        return wishlistStatus;
    }

    public void setWishlistStatus(Integer wishlistStatus) {
        this.wishlistStatus = wishlistStatus;
    }

    public Integer getHistoryStatus() {
        return historyStatus;
    }

    public void setHistoryStatus(Integer historyStatus) {
        this.historyStatus = historyStatus;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDislikes() {
        return dislikes;
    }

    public void setDislikes(String dislikes) {
        this.dislikes = dislikes;
    }

    public String getLikesFormatted() {
        return likesFormatted;
    }

    public void setLikesFormatted(String likesFormatted) {
        this.likesFormatted = likesFormatted;
    }

    public String getDislikesFormatted() {
        return dislikesFormatted;
    }

    public void setDislikesFormatted(String dislikesFormatted) {
        this.dislikesFormatted = dislikesFormatted;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMainVideo() {
        return mainVideo;
    }

    public void setMainVideo(String mainVideo) {
        this.mainVideo = mainVideo;
    }

    public Integer getIsSeries() {
        return isSeries;
    }

    public void setIsSeries(Integer isSeries) {
        this.isSeries = isSeries;
    }

    public Integer getShouldDisplayPpv() {
        return shouldDisplayPpv;
    }

    public void setShouldDisplayPpv(Integer shouldDisplayPpv) {
        this.shouldDisplayPpv = shouldDisplayPpv;
    }

    public Integer getPpvPageType() {
        return ppvPageType;
    }

    public void setPpvPageType(Integer ppvPageType) {
        this.ppvPageType = ppvPageType;
    }

    /*public List<PpvPageTypeContent> getPpvPageTypeContent() {
        return ppvPageTypeContent;
    }

    public void setPpvPageTypeContent(List<PpvPageTypeContent> ppvPageTypeContent) {
        this.ppvPageTypeContent = ppvPageTypeContent;
    }
*/
    public Integer getIsUserNeedSubscription() {
        return isUserNeedSubscription;
    }

    public void setIsUserNeedSubscription(Integer isUserNeedSubscription) {
        this.isUserNeedSubscription = isUserNeedSubscription;
    }

    /*public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }

    public List<Object> getCastCrews() {
        return castCrews;
    }

    public void setCastCrews(List<Object> castCrews) {
        this.castCrews = castCrews;
    }
*/
    public Integer getDownloadButtonStatus() {
        return downloadButtonStatus;
    }

    public void setDownloadButtonStatus(Integer downloadButtonStatus) {
        this.downloadButtonStatus = downloadButtonStatus;
    }

    public Integer getDownloadingVideoStatus() {
        return downloadingVideoStatus;
    }

    public void setDownloadingVideoStatus(Integer downloadingVideoStatus) {
        this.downloadingVideoStatus = downloadingVideoStatus;
    }

    public String getDownloadStatusText() {
        return downloadStatusText;
    }

    public void setDownloadStatusText(String downloadStatusText) {
        this.downloadStatusText = downloadStatusText;
    }

   /* public MainResolutions getMainResolutions() {
        return mainResolutions;
    }

    public void setMainResolutions(MainResolutions mainResolutions) {
        this.mainResolutions = mainResolutions;
    }

    public List<DownloadUrl> getDownloadUrls() {
        return downloadUrls;
    }

    public void setDownloadUrls(List<DownloadUrl> downloadUrls) {
        this.downloadUrls = downloadUrls;
    }*/

    public Integer getNextAdminVideoId() {
        return nextAdminVideoId;
    }

    public void setNextAdminVideoId(Integer nextAdminVideoId) {
        this.nextAdminVideoId = nextAdminVideoId;
    }

    public String getNextPlayerJson() {
        return nextPlayerJson;
    }

    public void setNextPlayerJson(String nextPlayerJson) {
        this.nextPlayerJson = nextPlayerJson;
    }

    public String getWatchCountFormatted() {
        return watchCountFormatted;
    }

    public void setWatchCountFormatted(String watchCountFormatted) {
        this.watchCountFormatted = watchCountFormatted;
    }

}
