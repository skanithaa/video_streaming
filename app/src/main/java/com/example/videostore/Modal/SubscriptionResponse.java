package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubscriptionResponse {



        @SerializedName("success")
        @Expose
        private Boolean success;
        @SerializedName("data")
        @Expose
        private List<SubscriptionData> subscriptionData = null;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public List<SubscriptionData> getData() {
            return subscriptionData;
        }

        public void setData(List<SubscriptionData> data) {
            this.subscriptionData = data;
        }

    }



