package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuggestionResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<SuggestionDataResponse> suggestionDataResponses = null;
    @SerializedName("total")
    @Expose
    private Integer total;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<SuggestionDataResponse> getSuggestionDataResponses() {
        return suggestionDataResponses;
    }

    public void SuggestionDataResponse(List<SuggestionDataResponse> suggestionDataResponses) {
        this.suggestionDataResponses = suggestionDataResponses;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
