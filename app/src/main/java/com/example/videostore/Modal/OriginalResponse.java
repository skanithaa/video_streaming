package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OriginalResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<OriginalDataResponse> originalDataResponses = null;
    @SerializedName("total")
    @Expose
    private Integer total;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<OriginalDataResponse> getOriginalDataResponses() {
        return originalDataResponses;
    }

    public void setOriginalDataResponses(List<OriginalDataResponse> originalDataResponses) {
        this.originalDataResponses = originalDataResponses;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
