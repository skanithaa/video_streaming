package com.example.videostore.Modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionData {

    @SerializedName("subscription_id")
    @Expose
    private Integer subscriptionId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("plan")
    @Expose
    private String plan;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("no_of_account")
    @Expose
    private Integer noOfAccount;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("popular_status")
    @Expose
    private Integer popularStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("plan_formatted")
    @Expose
    private String planFormatted;

    public Integer getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Integer subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getNoOfAccount() {
        return noOfAccount;
    }

    public void setNoOfAccount(Integer noOfAccount) {
        this.noOfAccount = noOfAccount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPopularStatus() {
        return popularStatus;
    }

    public void setPopularStatus(Integer popularStatus) {
        this.popularStatus = popularStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPlanFormatted() {
        return planFormatted;
    }

    public void setPlanFormatted(String planFormatted) {
        this.planFormatted = planFormatted;
    }

}
