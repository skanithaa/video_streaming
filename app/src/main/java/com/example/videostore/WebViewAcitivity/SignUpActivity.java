package com.example.videostore.WebViewAcitivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.videostore.Modal.LoginResponse;
import com.example.videostore.Modal.SignUpResponse;
import com.example.videostore.Modal.SignupUserDataResponse;
import com.example.videostore.Modal.UserLoginDataResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;
import com.example.videostore.Presenter.LoginPresenter;
import com.example.videostore.Presenter.SignUpPresenter;
import com.example.videostore.Presenter.SignUpView;
import com.example.videostore.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;

public class SignUpActivity extends AppCompatActivity implements SignUpView{

SharedPreferences sp;
  Button sign_up;
  SignUpPresenter signUpPresenter;
    APIInterface restMethods;
    static String UserToken = null;
    static int UserId = 0,sub_profile_id=0;
    Context context;
    Button signup,login;
    boolean isNameValid, isEmailValid, isPhoneValid, isPasswordValid;
    ProgressDialog progressDialog;
    TextInputEditText name,email,password,number;
    TextInputLayout email_error,password_error,name_error,number_error;
    String device_type,device_token,login_by,timezone,language;
    String Name,Email,Password,Number;
        @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sign_up);
            restMethods = RetrofitClient.buildHTTPClient();
            sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
            progressDialog=new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            signUpPresenter=new SignUpPresenter(this);
            //signUpPresenter=new SignUpPresenter(context);

            /*name="Demo User";
            email="user123@streamview.com";
            language= "en";
            timezone="Asia/calcuta";
            device_type="android";
            device_token="123456";
            login_by="manual";
            password = "123456";*/

            SharedPreferences sp=getApplicationContext().getSharedPreferences("MyUserPref",Context.MODE_PRIVATE);
           device_type= sp.getString("device_type",null);
            login_by=sp.getString("login_by",null);
            device_token=sp.getString("device_token",null);
           language=sp.getString("language","");




            email = (TextInputEditText) findViewById(R.id.email);
            password = (TextInputEditText) findViewById(R.id.password);
            name = (TextInputEditText) findViewById(R.id.name);
            number=(TextInputEditText) findViewById(R.id.number);

            name_error = (TextInputLayout) findViewById(R.id.name_error);
            password_error = (TextInputLayout) findViewById(R.id.password_error);
            email_error = (TextInputLayout) findViewById(R.id.email_error);
            number_error = (TextInputLayout) findViewById(R.id.number_error);
            signup=(Button) findViewById(R.id.signup_btn);
            login=(Button) findViewById(R.id.login_btn);

            signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SetValidation();
                }
            });

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(SignUpActivity.this,LogInActivity.class);
                    startActivity(intent);
                }
            });



        }







    /*public  void registerUser(Email, Password, Name, timezone, device_token, device_type, login_by, language){



        restMethods.userSignUp(Email, Password, Name, timezone, device_token, device_type, login_by, language).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(@NonNull Call<SignUpResponse> call, @NonNull Response<SignUpResponse> response) {




                    SignupUserDataResponse SignUpuserDataResponse;
                    SignUpuserDataResponse = response.body().getData();
                    UserId = SignUpuserDataResponse.getId();
                    UserToken = SignUpuserDataResponse.getToken();
                     sub_profile_id =SignUpuserDataResponse.getSubProfileId();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("token",UserToken);
                    editor.putInt("id", UserId);
                    editor.putInt("sub_profile_id", UserId);
                    editor.apply();
                    Intent intent=new Intent(SignUpActivity.this, MainActivity.class);

                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);

            }

            @Override
            public void onFailure(@NonNull Call<SignUpResponse> call, @NonNull Throwable t) {
                t.printStackTrace();



            }
        });
    }*/
    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }

    public void SetValidation() {

        // Check for a valid name.
        if (name.getText().toString().isEmpty()) {
            name_error.setError(getResources().getString(R.string.name_error));
            isNameValid = false;
        } else  {
            isNameValid = true;
            name_error.setErrorEnabled(false);
        }


        // Check for a valid email address.
        if (email.getText().toString().isEmpty()) {
            email_error.setError(getResources().getString(R.string.email_error));
            isEmailValid = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email_error.setError(getResources().getString(R.string.error_invalid_email));
            isEmailValid = false;
        } else  {
            isEmailValid = true;
            email_error.setErrorEnabled(false);
        }

        // Check for a valid password.
        if (password.getText().toString().isEmpty()) {
            password_error.setError(getResources().getString(R.string.password_error));
            isPasswordValid = false;
        } else if (password.getText().length() < 6) {
            password_error.setError(getResources().getString(R.string.error_invalid_password));
            isPasswordValid = false;
        } else  {
            isPasswordValid = true;
            password_error.setErrorEnabled(false);
        }

        // Check for a valid phone number.
        if (number.getText().toString().isEmpty()) {
            number_error.setError(getResources().getString(R.string.phone_error));
            isPhoneValid = false;
        } else  {
            isPhoneValid = true;
            number_error.setErrorEnabled(false);
        }

        // Check for a valid password.


        if (isNameValid && isEmailValid && isPhoneValid && isPasswordValid) {

             Name= name.getText().toString();
            Email= email.getText().toString();
            Password= password.getText().toString();
            Number=number.getText().toString();



            signUpPresenter.registerUser(Email, Password, Name, timezone, device_token, device_type, login_by, language);

        }

    }


    @Override
    public void showProgress() {
        progressDialog.show();

    }

    @Override
    public void hideProgress() {
progressDialog.hide();
    }

    @Override
    public void onAddSucess(String message) {
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(SignUpActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onAddFailure(String message) {
        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAddFailureResponse(ResponseBody responseBody) {
        Toast.makeText(context, responseBody.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getUserData(String token, int id,int sub_profile_id) {

        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("token",token);
        editor.putInt("id",id);
        editor.putInt("sub_profile_id", sub_profile_id);
        editor.apply();




    }
}

























        /*private void sign_up(){
        Name = name.getText().toString();
        Email = email.getText().toString();
        Password = password.getText().toString();
        PhoneNumber = number.getText().toString();
        if(Email.isEmpty()){
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(Email).matches()){
         email.setError("Email valid Email");
         email.requestFocus();
          return;}

       if(Password.isEmpty()){
        password.setError("password is required");
        password.requestFocus();
        return;
    }
        if(Password.length()<6){
            password.setError("password requires more than 6 characters");
            password.requestFocus();
            return;
        }

        if(Name.isEmpty()){
            name.setError("Name is required");
            name.requestFocus();
            return;
        }
        if(PhoneNumber.length()<11){
            password.setError("enter valid phone number");
            password.requestFocus();
            return;
        }
else {


            restMethods.userSignUp(Email, Password, Name, timezone, device_token, device_type, login_by, language).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(@NonNull Call<SignUpResponse> call, @NonNull Response<SignUpResponse> response) {

                    if (response.body().getSuccess()) {

                        SignupUserDataResponse SignUpuserDataResponse;
                        SignUpuserDataResponse = response.body().getData();
                        UserId = SignUpuserDataResponse.getId();
                        UserToken = SignUpuserDataResponse.getToken();
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("Token",UserToken);
                            editor.putInt("ID", UserId);
                            editor.apply();
                            Intent intent=new Intent(context, MainActivity.class);
                            startActivity(intent);
                    } else {

                        Toast.makeText(context, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SignUpResponse> call, @NonNull Throwable t) {
                    t.printStackTrace();


                }
            });
        }
            }

*/


