package com.example.videostore.WebViewAcitivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.videostore.Modal.OriginalDataResponse;
import com.example.videostore.Modal.OriginalResponse;
import com.example.videostore.Modal.VideoDataResponse;
import com.example.videostore.Modal.VideoResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;
import com.example.videostore.Presenter.MainActivityPresenter;
import com.example.videostore.Presenter.MovieUrlPresenter;
import com.example.videostore.Presenter.MovieUrlView;
import com.example.videostore.R;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetails extends AppCompatActivity implements MovieUrlView {
   MovieUrlPresenter  movieUrlPresenter;
   ImageView movieImage;
   Button playButton;
   ProgressDialog progressDialog;
   TextView movieName;
   SharedPreferences sp;
   String mMovieTitle,mMovieImage,videoUrl;

String token,device_type;
int id,video_type,admin_video_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        movieImage=findViewById(R.id.movie_image);
        playButton=findViewById(R.id.play_buttton);
        movieName=findViewById(R.id.movie_name);

        //Shared Preference
        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        SharedPreferences sharedpref=getApplicationContext().getSharedPreferences("MyUserPref",Context.MODE_PRIVATE);
        device_type = sharedpref.getString("device_type","");
        token = sharedpref.getString("token","");
        id = sharedpref.getInt("id",0);

        //progress Dialouge
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");


        video_type = getIntent().getIntExtra("video_type",0);
        admin_video_id = getIntent().getIntExtra("admin_video_id",0);
        mMovieTitle = getIntent().getStringExtra("movieTitle");
        mMovieImage=getIntent().getStringExtra("movieImage");


        // call to get video link
        movieUrlPresenter=new MovieUrlPresenter(this);
        movieUrlPresenter.getVideoUrl(id,token,admin_video_id,device_type);

        // load image
        Glide.with(this).load(mMovieImage).into(movieImage);
        movieName.setText(mMovieTitle);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 callvideo();

        }});

        }

    private void callvideo() {

         if(video_type == 1) {

                    Intent sendUrl = new Intent(MovieDetails.this, VideoPlayerActivity.class);
                    sendUrl.putExtra("url", videoUrl);
                    startActivity(sendUrl);
                }
                else if(video_type == 2){

                    Intent sendUrl = new Intent(MovieDetails.this, YouTubeActivity.class);
                    sendUrl.putExtra("url", videoUrl);
                    startActivity(sendUrl);
                }
    }

    @Override
    public void showProgress() {
     progressDialog.show();
    }

    @Override
    public void hideProgress() {
     progressDialog.hide();
    }

    @Override
    public void onAddSuccess(String message) {

    }

    @Override
    public void onAddFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAddFailureResponse(String responsebody) {
        Toast.makeText(this, responsebody.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getUserData(String Url) {


        videoUrl=Url;

        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("video_url",videoUrl);

        editor.apply();

    }


    }
