package com.example.videostore.WebViewAcitivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.videostore.Adapter.BannerMoviesPageAdapter;

//import com.example.videostore.Adapter.OriginalItemRecyclerAdapter;
import com.example.videostore.Adapter.OriginalItemRecyclerAdapter;
import com.example.videostore.Adapter.SuggestionRecyclerAdapter;
import com.example.videostore.Modal.BannerMovies;
import com.example.videostore.Modal.OriginalDataResponse;
import com.example.videostore.Modal.SuggestionDataResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;
import com.example.videostore.Presenter.MainActivityPresenter;
import com.example.videostore.Presenter.MainActivityView;
import com.example.videostore.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;


public class MainActivity extends AppCompatActivity implements MainActivityView {
    BannerMoviesPageAdapter bannerMoviesPageAdapter;

    TabLayout indicatorTab,categoryTab;

    List<BannerMovies> bannerMoviesList;
    List<OriginalDataResponse> originalDataResponses;
    Timer sliderTimer;
    ViewPager bannerMoviesViewPager;
    ProgressDialog progressDialog;
    SharedPreferences sp;

 MainActivityPresenter mainActivityPresenter;

    NestedScrollView nestedScrollView;
    AppBarLayout appBarLayout;
    APIInterface restMethods;
    String skip;
    int id,sub_profile_id;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        restMethods = RetrofitClient.buildHTTPClient();
        indicatorTab=findViewById(R.id.tab_indicator);

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");

        //sharedPreference
        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        SharedPreferences sp=getApplicationContext().getSharedPreferences("MyUserPref",Context.MODE_PRIVATE);
        id = sp.getInt("id",0);
        sub_profile_id = sp.getInt("sub_profile_id",0);
        token = sp.getString("token","");
        skip=sp.getString("skip","");


        originalDataResponses=new ArrayList<>();
        bannerMoviesList=new ArrayList<>();
        mainActivityPresenter=new MainActivityPresenter(this);
        mainActivityPresenter.getBannerMoviesList(id,token,sub_profile_id,skip);
         mainActivityPresenter.getOriginalMovies(id,token,sub_profile_id,skip);
        mainActivityPresenter.getSuggestionResponse(id,token,sub_profile_id,skip);

    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
    progressDialog.hide();
    }

    @Override
    public void onAddSucessbanner(String message) {

    }

    @Override
    public void onAddFailureResponse(ResponseBody responsebody) {
        Toast.makeText(getBaseContext(), responsebody.toString(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onAddFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getBannerMovies(List<BannerMovies> bannerMoviesList) {

        setBannerMoviesPageAdapter( bannerMoviesList);

    }

    @Override
    public void getSuggestedMovies(List<SuggestionDataResponse> suggestionDataResponseList) {
        SuggestionRecyclerView(suggestionDataResponseList);

    }

    @Override
    public void getOriginalMovies(List<OriginalDataResponse> originalDataResponses) {

        originalRecyclerView(originalDataResponses);
    }

    //auto slide starts here in banner
    class SliderTimer extends TimerTask {

        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (bannerMoviesViewPager.getCurrentItem() > bannerMoviesList.size()-1){

                        bannerMoviesViewPager.setCurrentItem(bannerMoviesViewPager.getCurrentItem()+1);
                    }
                    else{
                        bannerMoviesViewPager.setCurrentItem(0);
                    }
                }
            });
        }

    }  //auto side ends here*/

    private void setBannerMoviesPageAdapter(List<BannerMovies> bannerMoviesList){

        bannerMoviesViewPager=findViewById(R.id.banner_viewPager);
        bannerMoviesPageAdapter=new BannerMoviesPageAdapter(this, bannerMoviesList);
        bannerMoviesViewPager.setAdapter(bannerMoviesPageAdapter);
        indicatorTab.setupWithViewPager(bannerMoviesViewPager);
        Timer timer=new Timer();

        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);



    }

    public void originalRecyclerView(List<OriginalDataResponse> originalDataResponses){
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this,RecyclerView.HORIZONTAL ,false);
        RecyclerView recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        OriginalItemRecyclerAdapter adapter=new OriginalItemRecyclerAdapter(this,originalDataResponses);
        recyclerView.setAdapter(adapter);

    }


    public void SuggestionRecyclerView(List<SuggestionDataResponse> suggestionDataResponses){

        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this,RecyclerView.HORIZONTAL ,false);
        RecyclerView recyclerView=findViewById(R.id.recyclerView2);
        recyclerView.setLayoutManager(layoutManager);
        SuggestionRecyclerAdapter adapter = new SuggestionRecyclerAdapter(this,suggestionDataResponses);
        recyclerView.setAdapter(adapter);
    }
    //nested scroll
    private void setScrollDefaultState(){
        nestedScrollView.fullScroll(View.FOCUS_UP);
        nestedScrollView.scrollTo(0,0);
        appBarLayout.setExpanded(true);
    }

}