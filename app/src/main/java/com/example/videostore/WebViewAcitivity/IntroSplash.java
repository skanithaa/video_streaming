package com.example.videostore.WebViewAcitivity;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.videostore.R;

public class IntroSplash extends AppCompatActivity {



SharedPreferences sp;
    Thread timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_splash);



        //default values
        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("device_type","android");
        editor.putString("device_token","123456");
        editor.putString("login_by","manual");
        editor.putString("language","en");

        editor.putString("skip","0");
        editor.commit();


        timer = new Thread(){
            @Override
            public void run(){
                try{
                    synchronized (this) {
                        wait(5000);

                    }
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally {
                  Intent intent=new Intent(IntroSplash.this,LogInActivity.class);
                  startActivity(intent);
                   finish();
                }
            }
        };
        timer.start();
    }
}





