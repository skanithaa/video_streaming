package com.example.videostore.WebViewAcitivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;
import com.example.videostore.Presenter.LoginPresenter;
import com.example.videostore.Presenter.LoginView;
import com.example.videostore.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import okhttp3.ResponseBody;

import static android.content.ContentValues.TAG;

public class LogInActivity extends AppCompatActivity implements LoginView {
    TextInputEditText email , password;
    TextInputLayout email_error,password_error;
    TextView Register;
    Button LogIn;
    ProgressDialog progressDialog;

    APIInterface restMethods;
    String device_type,device_token,login_by,timezone,language;
    boolean  isEmailValid, isPasswordValid;
    String Email,Password;

    Context context;
   SharedPreferences sp;
    LoginPresenter loginPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        loginPresenter =new LoginPresenter(this);


        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        SharedPreferences sp=getApplicationContext().getSharedPreferences("MyUserPref",Context.MODE_PRIVATE);
        device_type = sp.getString("device_type","");
        device_token = sp.getString("device_token","");
        login_by = sp.getString("login_by","");


        restMethods = RetrofitClient.buildHTTPClient();
        progressDialog=new ProgressDialog(this);
          progressDialog.setMessage("Please wait...");

        //define xml values
        Register=findViewById(R.id.register);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        LogIn = findViewById(R.id.login);
        password_error = (TextInputLayout) findViewById(R.id.password_error);
        email_error = (TextInputLayout) findViewById(R.id.email_error);


        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(LogInActivity.this,SignUpActivity.class);
                startActivity(intent);
            }
        });


        LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidation();

            }


        });
    }

    private void SetValidation() {

        if (email.getText().toString().isEmpty()) {
            email_error.setError(getResources().getString(R.string.email_error));
            isEmailValid = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email_error.setError(getResources().getString(R.string.error_invalid_email));
            isEmailValid = false;
        } else {
            isEmailValid = true;
            email_error.setErrorEnabled(false);
        }

        // Check for a valid password.
        if (password.getText().toString().isEmpty()) {
            password_error.setError(getResources().getString(R.string.password_error));
            isPasswordValid = false;
        } else if (password.getText().length() < 6) {
            password_error.setError(getResources().getString(R.string.error_invalid_password));
            isPasswordValid = false;
        } else {
            isPasswordValid = true;
            password_error.setErrorEnabled(false);
        }

        if (isEmailValid && isPasswordValid) {


            Email = email.getText().toString();
            Password = password.getText().toString();

            loginPresenter.LoginUser(Email,Password,device_token,device_type,login_by);
        }
    }

    @Override
    public void showProgress() {
progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();

    }

    @Override
    public void onAddSucess(String message) {

        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);

    }

    @Override
    public void onAddFailure(String message) {
       Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
       cleartext();

    }

    @Override
    public void onAddFailureResponse(ResponseBody responsebody) {
        Toast.makeText(context, responsebody.toString(), Toast.LENGTH_SHORT).show();
        cleartext();
    }

    private void cleartext() {

        email.getText().clear();
        password.getText().clear();
    }

    @Override
    public void getUserData(String token, int id) {

          SharedPreferences sp= getApplicationContext().getSharedPreferences("MyUserPref",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("token", token);
                    editor.putInt("id", id);
                    editor.apply();
        Log.d(TAG, "getList: "+token);
    }



   /* private void LoginUser() {


        restMethods.userLogin(Email,Password,device_token,device_type,login_by).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {


                try {

                    UserLoginDataResponse userLoginDataResponse;

                    userLoginDataResponse = response.body().getData();

                     UserId = userLoginDataResponse.getId();
                     UserToken = userLoginDataResponse.getToken();


                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("token", UserToken);
                    editor.putInt("id", UserId);
                    editor.apply();

                    Intent intent=new Intent(LogInActivity.this, MainActivity.class);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                }
                catch (Exception e){
                    Log.e(TAG, "onResponse: "+e );
                }
            }


            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                t.printStackTrace();

                //Response failed
                Log.e(TAG, "Response: " + t.getMessage());


            }
        });
    }*/

}



