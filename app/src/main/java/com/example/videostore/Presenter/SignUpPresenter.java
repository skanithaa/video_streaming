package com.example.videostore.Presenter;

import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.example.videostore.Modal.SignUpResponse;
import com.example.videostore.Modal.SignupUserDataResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;
import com.example.videostore.WebViewAcitivity.MainActivity;
import com.example.videostore.WebViewAcitivity.SignUpActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter {
    public SignUpPresenter(SignUpView signUpView) {
        this.signUpView = signUpView;
    }

    private SignUpView signUpView;

    APIInterface apiInterface= RetrofitClient.buildHTTPClient();





    public void registerUser(String Email,String Password,String Name, String timezone, String device_token, String device_type, String login_by, String language){
        signUpView.showProgress();

        apiInterface.userSignUp(Email, Password, Name, timezone, device_token, device_type, login_by, language).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(@NonNull Call<SignUpResponse> call, @NonNull Response<SignUpResponse> response) {
           signUpView.hideProgress();
           if(response.isSuccessful() && response.body()!=null) {
               Boolean success = response.body().getSuccess();
               if (success) {
                   signUpView.onAddSucess(response.body().getMessage());

                   SignupUserDataResponse SignUpuserDataResponse;
                   SignUpuserDataResponse = response.body().getData();
                   signUpView.getUserData(SignUpuserDataResponse.getToken(), SignUpuserDataResponse.getId(), SignUpuserDataResponse.getSubProfileId());
               }
           }
           else{
               signUpView.onAddFailureResponse(response.errorBody());
           }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                signUpView.hideProgress();
                signUpView.onAddFailure(t.getLocalizedMessage());



            }
        });
    }

}
