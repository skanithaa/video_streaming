package com.example.videostore.Presenter;

import com.example.videostore.Modal.BannerMovies;
import com.example.videostore.Modal.OriginalDataResponse;
import com.example.videostore.Modal.SuggestionDataResponse;

import java.util.List;

import okhttp3.ResponseBody;

public interface MainActivityView {

    void showProgress();
    void hideProgress();
    void onAddSucessbanner(String message);
    void onAddFailureResponse(ResponseBody responsebody);
    void onAddFailure(String message);
    void getBannerMovies(List<BannerMovies> bannerMoviesList);
    void getSuggestedMovies(List<SuggestionDataResponse> suggestionDataResponses);
    void getOriginalMovies(List<OriginalDataResponse>originalDataResponses);

}
