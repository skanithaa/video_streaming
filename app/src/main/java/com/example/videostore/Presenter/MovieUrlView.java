package com.example.videostore.Presenter;

import okhttp3.ResponseBody;

public interface MovieUrlView {
    void showProgress();
    void hideProgress();
    void onAddSuccess(String message);
    void onAddFailure(String message);
    void onAddFailureResponse(String responsebody);
    void getUserData(String videoUrl);

}
