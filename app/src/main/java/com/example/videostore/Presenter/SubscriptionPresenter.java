package com.example.videostore.Presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.videostore.Modal.BannerMovies;
import com.example.videostore.Modal.LoginResponse;
import com.example.videostore.Modal.SubscriptionData;
import com.example.videostore.Modal.SubscriptionResponse;
import com.example.videostore.Modal.UserLoginDataResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SubscriptionPresenter {
    public SubscriptionPresenter(com.example.videostore.Presenter.subscriptionView subscriptionView) {
        this.subscriptionView = subscriptionView;
    }

    subscriptionView subscriptionView;
    APIInterface apiInterface= RetrofitClient.buildHTTPClient();


    public void subscription(int id,String token,int sub_profile_id,String device_token,String device_type,String login_by,String language) {

        subscriptionView.showProgress();
        apiInterface.subscriptionPlan(id,token,sub_profile_id,device_token,device_type,login_by,language).enqueue(new Callback<SubscriptionResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubscriptionResponse> call, @NonNull Response<SubscriptionResponse> response) {
                subscriptionView.hideProgress();

                if(response.body()!=null && response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {

                        List<SubscriptionData> subscriptionData = response.body().getData();
                        subscriptionView.getSubscriptionData(subscriptionData);




                        ;
                    }
                }
                else{
                    subscriptionView.hideProgress();
                    subscriptionView.onAddFailureResponse(response.errorBody());
                }
            }




            @Override
            public void onFailure(@NonNull Call<SubscriptionResponse> call, @NonNull Throwable t) {
                subscriptionView.hideProgress();
                subscriptionView.onAddFailure(t.getLocalizedMessage());

                //Response failed
                Log.e(TAG, "Response: " + t.getMessage());


            }
        });
    }

}
