package com.example.videostore.Presenter;

import com.example.videostore.Modal.SubscriptionData;

import java.util.List;

import okhttp3.ResponseBody;

public interface subscriptionView {
    void showProgress();
    void hideProgress();
    void onAddFailureResponse(ResponseBody responseBody);
    void onAddSuccess(String message);
    void onAddFailure(String message);
    void getSubscriptionData(List<SubscriptionData> subscriptionData);
}
