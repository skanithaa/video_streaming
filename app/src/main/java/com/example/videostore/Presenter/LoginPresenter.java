package com.example.videostore.Presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.videostore.Modal.LoginResponse;
import com.example.videostore.Modal.UserLoginDataResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class LoginPresenter {

    private LoginView loginView;
    APIInterface apiInterface=RetrofitClient.buildHTTPClient();
    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

   public void LoginUser(String Email,String Password,String device_token,String device_type,String login_by) {

       loginView.showProgress();
       apiInterface.userLogin(Email,Password,device_token,device_type,login_by).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                loginView.hideProgress();

                if(response.body()!=null && response.isSuccessful()) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        loginView.onAddSucess(response.body().getMessage());
                        UserLoginDataResponse userLoginDataResponse;

                        userLoginDataResponse = response.body().getData();
                        Log.d(TAG, "onResponse: " + userLoginDataResponse);


                        loginView.getUserData(userLoginDataResponse.getToken(), userLoginDataResponse.getId());
                    }
                }
                else{
                    loginView.onAddFailureResponse(response.errorBody());
                }
            }




            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                loginView.hideProgress();
                loginView.onAddFailure(t.getLocalizedMessage());

                //Response failed
                Log.e(TAG, "Response: " + t.getMessage());


            }
        });
    }
}

