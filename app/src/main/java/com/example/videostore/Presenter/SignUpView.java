package com.example.videostore.Presenter;

import okhttp3.ResponseBody;

public interface SignUpView {

    void showProgress();
    void hideProgress();
    void onAddSucess(String message);
    void onAddFailure(String message);
    void onAddFailureResponse(ResponseBody responseBody);
    void getUserData(String token, int id,int sub_profile_id);


}
