package com.example.videostore.Presenter;

import android.util.Log;
import android.widget.Toast;

import com.example.videostore.Adapter.BannerMoviesPageAdapter;
import com.example.videostore.Modal.BannerMovies;
import com.example.videostore.Modal.BannerResponse;
import com.example.videostore.Modal.OriginalDataResponse;
import com.example.videostore.Modal.OriginalResponse;
import com.example.videostore.Modal.SuggestionDataResponse;
import com.example.videostore.Modal.SuggestionResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;
import com.example.videostore.WebViewAcitivity.MainActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class MainActivityPresenter {

    public MainActivityPresenter(MainActivityView mainActivityView) {
        this.mainActivityView = mainActivityView;
    }

    MainActivityView mainActivityView;
    APIInterface apiInterface= RetrofitClient.buildHTTPClient();

    public void getBannerMoviesList(int id, String token, int sub_profile_id, String skip) {
mainActivityView.showProgress();
        apiInterface.bannerList(id,token,sub_profile_id,skip).enqueue(new Callback<BannerResponse>(){
            @Override
            public void onResponse(Call<BannerResponse> call, Response<BannerResponse> response) {
mainActivityView.hideProgress();

                if(response.isSuccessful() && response.body()!=null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        List<BannerMovies> bannerMoviesList = response.body().getData();
                        Log.d(TAG, "onResponse: " + bannerMoviesList.size());
                        mainActivityView.getBannerMovies(bannerMoviesList);

                    }
                }
                else{

                    mainActivityView.onAddFailureResponse(response.errorBody());
                }

            }
            @Override
            public void onFailure(Call<BannerResponse> call, Throwable t) {

                mainActivityView.onAddFailure(t.getLocalizedMessage());

            }
        });

    }

    // SUGGESTIONS

    public void getSuggestionResponse(int id, String token, int sub_profile_id, String skip) {
mainActivityView.showProgress();
        apiInterface.suggestionResponse(id,token,sub_profile_id,skip).enqueue(new Callback<SuggestionResponse>(){
            @Override
            public void onResponse(Call<SuggestionResponse> call, Response<SuggestionResponse> response) {
               mainActivityView.hideProgress();
                if(response.isSuccessful() && response.body()!=null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        List<SuggestionDataResponse> suggestionDataResponses = response.body().getSuggestionDataResponses();
                        mainActivityView.getSuggestedMovies(suggestionDataResponses);
                    }
                    //   SuggestionRecyclerView(suggestionDataResponses);
                }else{
                    mainActivityView.onAddFailureResponse(response.errorBody());
                }



            }

            @Override
            public void onFailure(Call<SuggestionResponse> call, Throwable t) {
                mainActivityView.onAddFailure(t.getLocalizedMessage());
            }
        });

    }


    // original
    public void getOriginalMovies(int id, String token, int sub_profile_id, String skip) {
mainActivityView.showProgress();
        apiInterface.originalList(id,token,sub_profile_id,skip).enqueue(new Callback<OriginalResponse>(){
            @Override
            public void onResponse(Call<OriginalResponse> call, Response<OriginalResponse> response) {
                mainActivityView.hideProgress();
                if(response.isSuccessful() && response.body()!=null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        List<OriginalDataResponse> originalDataResponses = response.body().getOriginalDataResponses();
                        mainActivityView.getOriginalMovies(originalDataResponses);
                    }
                }
                else{
                    mainActivityView.onAddFailureResponse(response.errorBody());

                }
            }

            @Override
            public void onFailure(Call<OriginalResponse> call, Throwable t) {
                mainActivityView.onAddFailure(t.getLocalizedMessage());
            }
        });

    }



}
