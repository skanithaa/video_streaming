package com.example.videostore.Presenter;

import okhttp3.ResponseBody;

public interface LoginView {

    void showProgress();
    void hideProgress();
    void onAddSucess(String message);
    void onAddFailure(String message);
    void onAddFailureResponse(ResponseBody responsebody);
    void getUserData(String token, int id);



}
