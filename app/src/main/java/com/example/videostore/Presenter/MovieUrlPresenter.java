package com.example.videostore.Presenter;

import com.example.videostore.Modal.VideoDataResponse;
import com.example.videostore.Modal.VideoResponse;
import com.example.videostore.Network.APIInterface;
import com.example.videostore.Network.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieUrlPresenter {

    MovieUrlView movieUrlView;
    APIInterface apiInterface=RetrofitClient.buildHTTPClient();
    public MovieUrlPresenter(MovieUrlView movieUrlView) {
        this.movieUrlView = movieUrlView;
    }


    public void getVideoUrl(int id, String token, int admin_video_id, String device_type) {
        movieUrlView.showProgress();
        apiInterface.getVideoLink(id,token,admin_video_id,device_type).enqueue(new Callback<VideoResponse>(){
            @Override
            public void onResponse(Call<VideoResponse> call, Response<VideoResponse> response) {
                movieUrlView.hideProgress();
                if(response.isSuccessful() && response.body()!=null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {
                        VideoDataResponse videoDataResponse = response.body().getData();
                        String videoUrl = videoDataResponse.getMainVideo();
                      //  Integer VideoType = videoDataResponse.getVideoType();
                        movieUrlView.getUserData(videoUrl);
                    }
                    else{
                        movieUrlView.onAddFailureResponse(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<VideoResponse> call, Throwable t) {
                movieUrlView.hideProgress();
                movieUrlView.onAddFailure(t.getLocalizedMessage());
            }
        });

    }

}
