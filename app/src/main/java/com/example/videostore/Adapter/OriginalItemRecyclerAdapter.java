package com.example.videostore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.videostore.Modal.OriginalDataResponse;
import com.example.videostore.R;
import com.example.videostore.WebViewAcitivity.MovieDetails;

import java.util.List;

import static android.content.ContentValues.TAG;

public class OriginalItemRecyclerAdapter extends RecyclerView.Adapter<OriginalItemRecyclerAdapter.ItemViewHolder>{

    Context context;
    List<OriginalDataResponse> originalDataResponses;

    public OriginalItemRecyclerAdapter(Context context, List<OriginalDataResponse> originalDataResponses) {
        this.context = context;
        this.originalDataResponses = originalDataResponses;
    }

    @NonNull

    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.cat_recycler_row_items,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull OriginalItemRecyclerAdapter.ItemViewHolder holder, int position) {
        Glide.with(context).load(originalDataResponses.get(position).getMobileImage()).into(holder.itemImage);
        holder.itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, MovieDetails.class);
                i.putExtra("video_type", originalDataResponses.get(position).getVideoType());
                i.putExtra("admin_video_id", originalDataResponses.get(position).getAdminVideoId());
                i.putExtra("movieTitle", originalDataResponses.get(position).getTitle());
                i.putExtra("movieImage", originalDataResponses.get(position).getMobileImage());

                /*i.putExtra("MovieName",originalDataResponses.get(position).getTitle());
                i.putExtra("MovieImage",originalDataResponses.get(position).getMobileBannerImage());*/
              //  i.putExtra("MovieFile",originalDataResponses.get(position).getFileURL());

                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return originalDataResponses.size();
    }

    public static final class ItemViewHolder extends RecyclerView.ViewHolder{

        ImageView itemImage;
        public ItemViewHolder(@NonNull View itemView) {

            super(itemView);

            itemImage=itemView.findViewById(R.id.item_image);
            Log.d(TAG, "MainViewHolder: ------------------------->");
        }


    }









}
