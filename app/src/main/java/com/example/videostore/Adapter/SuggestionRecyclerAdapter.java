package com.example.videostore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.videostore.Modal.OriginalDataResponse;
import com.example.videostore.Modal.SuggestionDataResponse;
import com.example.videostore.R;
import com.example.videostore.WebViewAcitivity.MovieDetails;

import java.util.List;

import static android.content.ContentValues.TAG;

public class SuggestionRecyclerAdapter extends RecyclerView.Adapter<SuggestionRecyclerAdapter.ItemViewHolder>{

        Context context;
        List<SuggestionDataResponse> suggestionDataResponses;

    public SuggestionRecyclerAdapter(Context context, List<SuggestionDataResponse> suggestionDataResponses) {
            this.context = context;
            this.suggestionDataResponses = suggestionDataResponses;
        }

        @NonNull

        @Override
        public SuggestionRecyclerAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new SuggestionRecyclerAdapter.ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.cat_recycler_row_items,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull SuggestionRecyclerAdapter.ItemViewHolder holder, int position) {
            Glide.with(context).load(suggestionDataResponses.get(position).getMobileImage()).into(holder.itemImage);
            holder.itemImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(context, MovieDetails.class);

                    i.putExtra("video_type", suggestionDataResponses.get(position).getVideoType());
                    i.putExtra("admin_video_id", suggestionDataResponses.get(position).getAdminVideoId());
                    i.putExtra("movieTitle", suggestionDataResponses.get(position).getTitle());
                    i.putExtra("movieImage", suggestionDataResponses.get(position).getMobileImage());

                    /*i.putExtra("MovieName",suggestionDataResponses.get(position).getTitle());
                    i.putExtra("MovieImage",suggestionDataResponses.get(position).getMobileBannerImage());*/
                    //  i.putExtra("MovieFile",originalDataResponses.get(position).getFileURL());

                    context.startActivity(i);
                }
            });
        }

        @Override
        public int getItemCount() {
            return suggestionDataResponses.size();
        }

        public static final class ItemViewHolder extends RecyclerView.ViewHolder{

            ImageView itemImage;
            public ItemViewHolder(@NonNull View itemView) {

                super(itemView);

                itemImage=itemView.findViewById(R.id.item_image);
                Log.d(TAG, "MainViewHolder: ------------------------->");
            }


        }



    }
