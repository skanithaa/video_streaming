package com.example.videostore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.videostore.Modal.BannerMovies;
import com.example.videostore.R;
import com.example.videostore.WebViewAcitivity.MovieDetails;

import java.util.List;

public class BannerMoviesPageAdapter  extends PagerAdapter {

    Context context;
    List<BannerMovies> bannerMoviesList;

    public BannerMoviesPageAdapter(Context context,List<BannerMovies> bannerMoviesList){
        this.context=context;
        this.bannerMoviesList = bannerMoviesList;


    }
    @Override
    public int getCount() {
        return bannerMoviesList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View)object);
    }



     @NonNull
    @Override
    public Object instantiateItem( ViewGroup container, int position) {

        View view= LayoutInflater.from(context).inflate(R.layout.banner_movie_layout,null);
        ImageView bannerImage=view.findViewById(R.id.banner_image);


     Glide.with(context).load(bannerMoviesList.get(position).getMobileImage()).into(bannerImage);

        container.addView(view);
        bannerImage.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                Intent intent= new Intent(context, MovieDetails.class);
                intent.putExtra("video_type", bannerMoviesList.get(position).getVideoType());
                intent.putExtra("admin_video_id", bannerMoviesList.get(position).getAdminVideoId());
                intent.putExtra("movieTitle", bannerMoviesList.get(position).getTitle());
                intent.putExtra("movieImage", bannerMoviesList.get(position).getMobileImage());


                   context.startActivity(intent);
            }
        });


        return view;


     }


}
