package com.example.videostore.Network;


import com.example.videostore.Modal.LoginResponse;
import com.example.videostore.Modal.BannerResponse;
import com.example.videostore.Modal.OriginalResponse;
import com.example.videostore.Modal.SignUpResponse;
import com.example.videostore.Modal.SubscriptionResponse;
import com.example.videostore.Modal.SuggestionResponse;
import com.example.videostore.Modal.VideoResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIInterface {


    @FormUrlEncoded
    @POST("userApi/new-releases")
    Call<BannerResponse> bannerList(
            @Field("id") int id,
            @Field("token") String password,
            @Field("sub_profile_id") int sub_profile_id,
            @Field("skip") String skip

        );

    @FormUrlEncoded
    @POST("userApi/originals")
    Call<OriginalResponse> originalList(
            @Field("id") int id,
            @Field("token") String password,
            @Field("sub_profile_id") int sub_profile_id,
            @Field("skip") String skip

    );

    @FormUrlEncoded
    @POST("userApi/suggestions")
    Call<SuggestionResponse> suggestionResponse(
            @Field("id") int id,
            @Field("token") String password,
            @Field("sub_profile_id") int sub_profile_id,
            @Field("skip") String skip

    );

     @FormUrlEncoded
    @POST("userApi/v4/login")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type,
            @Field("login_by") String login_by
    );


    @FormUrlEncoded
    @POST("userApi/v4/register")
    Call<SignUpResponse> userSignUp(
            @Field("email") String email,
            @Field("password") String password,
            @Field("name") String name,
            @Field("timezone") String timezone,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type,
            @Field("login_by") String login_by,
            @Field("language") String language
    );
    @FormUrlEncoded
    @POST("userApi/videos/view")
    Call<VideoResponse> getVideoLink(
            @Field("id") int id,
            @Field("token") String token,
            @Field("admin_video_id") int admin_video_id,
            @Field("device_type") String device_type

    );


    @FormUrlEncoded
    @POST("userApi/subscription_plans")
    Call<SubscriptionResponse> subscriptionPlan(
            @Field("id") int id,
            @Field("password") String token,
            @Field("sub_profile_id") int sub_profile_id,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type,
            @Field("login_by") String login_by,
            @Field("language") String language
    );
}